<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/dist/app.css">
    <title>coucou</title>
</head>
<body>
    
<header class="header-container background-image">
    <div class="global-header-top">
        <div class="header-left">
            <img src="<?php echo get_template_directory_uri() ?>/assets/img/logo_UL.png" class="img-responsive-header-left" alt="">
            <img src="<?php echo get_template_directory_uri()?>/assets/img/logo_INSA.png" class="img-responsive-header-left" alt="">
        </div>
        <div class="header-right">
            <img src="<?php echo get_template_directory_uri()?>/assets/img/user_icon.png" class="img-responsive-header icon-user"alt="">
            <a href="#" class="redirect-account">My account</a>
            <img src="<?php echo get_template_directory_uri()?>/assets/img/topbar.png" class="img-responsive-header social-network"alt="">
        </div>
    </div>
    <div class="header-text-center">
        <h1 class="header-title-center">Want to join INSA Lyon <br>
        to take part in exchange program ?</h1>
        <h3 class="header-subtitle-center">Start configuring your "à la carte" program</h3>
        <button class="header-button-start">START</button>
        <p class="header-subtitle-start-button">You already have a project ? Please login <a href="#" class="btn-a-login-header"><strong>Here</strong></a></p>
    </div>
    <div class="grey-triangle"></div>
</header>


const mix = require("laravel-mix");
mix.sass("assets/scss/app.scss", "assets/dist/")
.options({
    processCssUrls: false,
});
<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'intervention' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '}Z008>IC:iHRkLQX6}dl$f,a:l5R|V~dVl.=jyN;xPQk,[TkTp0mzN0AnN+8Dg|X' );
define( 'SECURE_AUTH_KEY',  'KQ]i[]KImRvzWY$DK;h^)IGhKb~a 2UU9~nm/.9d#i<9s&nVV5A:e8qV=B}{D@v.' );
define( 'LOGGED_IN_KEY',    'Est(%?FVC;> VDxOl,%PejTa wOFP<#|BFSwn>Q(Di<rSf:ZA;s+2F7=Ug=*Q-3B' );
define( 'NONCE_KEY',        '7kRF,=h@/uTbE^SsjPHq_^ x8H]2`8V1)>qX$2>e;}^W.sn?*;sqORC]!r8[%K=@' );
define( 'AUTH_SALT',        'Td]i|k!*:5x+c5cG=CQOl_X#=_T?%W^g8M/2Zr2oosXy-.TbckAkahp%y~.|LRD-' );
define( 'SECURE_AUTH_SALT', 'pVybfQ!{g}Qkw-l)!hQ` )G<kmEqI1+](Nn%`x]hd,9j1PI0vbr3KuADleuY_~qQ' );
define( 'LOGGED_IN_SALT',   'i3Ve:+fbaUzgee2j=vHZ05/a9 bXU0HO+VN^a6%[=M,JOnrI7V,Ez4b[`#1cA]KB' );
define( 'NONCE_SALT',       'he-Swn#n9.R8fZ@) 5EZp|C35:W44nm%Eag8ApoE0OA9AEB<LMICEjRw|6])2#hF' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
